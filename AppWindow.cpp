#include <QtWidgets>
#include <iostream>
#include "AppWindow.hpp"

AppWindow::AppWindow(SceneNode* sceneRoot) {
    setWindowTitle("488 Assignment Two");

    QGLFormat glFormat;
    glFormat.setVersion(3,0);
    glFormat.setProfile(QGLFormat::CoreProfile);
    glFormat.setSampleBuffers(true);

    QVBoxLayout *layout = new QVBoxLayout;
    m_viewer = new Viewer(glFormat, sceneRoot, this);

    layout->addWidget(m_viewer);
    setCentralWidget(new QWidget);
    centralWidget()->setLayout(layout);

    createActions();
}

void AppWindow::createActions() {
    // Creates a new action for quiting and pushes it onto the menu actions vector 

    /***************APPLICATION MENU*******************/
    QAction* quitAct = new QAction(tr("&Quit"), this);
    quitAct->setStatusTip(tr("Exits the file"));
    quitAct->setShortcut(QKeySequence(Qt::Key_Q));
    connect(quitAct, SIGNAL(triggered()), this, SLOT(close()));

    QAction* resetPositionAct = new QAction(tr("Reset Pos&ition"), this);
    resetPositionAct->setStatusTip(tr("Reset Position"));
    resetPositionAct->setShortcut(QKeySequence(Qt::Key_I));
    connect(resetPositionAct, SIGNAL(triggered()), this, SLOT(reset_position()));

    QAction* resetOrientationAct = new QAction(tr("Reset Orientation"), this);
    resetOrientationAct->setStatusTip(tr("Reset Orientation"));
    resetOrientationAct->setShortcut(QKeySequence(Qt::Key_O));
    connect(resetOrientationAct, SIGNAL(triggered()), this, SLOT(reset_orientation()));

    QAction* resetJointsAct = new QAction(tr("Reset Joints"), this);
    resetJointsAct->setStatusTip(tr("Reset Joints"));
    resetJointsAct->setShortcut(QKeySequence(Qt::Key_N));
    connect(resetJointsAct, SIGNAL(triggered()), this, SLOT(reset_joints()));

    QAction* resetAllAct = new QAction(tr("Reset All"), this);
    resetAllAct->setStatusTip(tr("Reset All"));
    resetAllAct->setShortcut(QKeySequence(Qt::Key_A));
    connect(resetAllAct, SIGNAL(triggered()), this, SLOT(reset_all()));

    m_menu_app = menuBar()->addMenu(tr("&Application"));

    m_menu_app->addAction(resetPositionAct);
    m_menu_app->addAction(resetOrientationAct);
    m_menu_app->addAction(resetJointsAct);
    m_menu_app->addAction(resetAllAct);
    m_menu_app->addAction(quitAct);
    /**************************************************/

    /***************MODE MENU*******************/
    QAction* positionModeAct = new QAction(tr("&Position/Orientation"), this);
    positionModeAct->setStatusTip(tr("Position/Orientation"));
    positionModeAct->setShortcut(QKeySequence(Qt::Key_P));
    connect(positionModeAct, SIGNAL(triggered()), this, SLOT(set_mode_position()));

    QAction* jointModeAct = new QAction(tr("&Joints"), this);
    jointModeAct->setStatusTip(tr("Joints"));
    jointModeAct->setShortcut(QKeySequence(Qt::Key_J));
    connect(jointModeAct, SIGNAL(triggered()), this, SLOT(set_mode_joint()));

    QActionGroup* modeGroup = new QActionGroup(this);
    positionModeAct->setActionGroup(modeGroup);
    jointModeAct->setActionGroup(modeGroup);

    modeGroup->setExclusive(true);

    positionModeAct->setCheckable(true);
    jointModeAct->setCheckable(true);

    positionModeAct->setChecked(true);
    set_mode_position();

    m_menu_mode = menuBar()->addMenu(tr("&Mode"));
    m_menu_mode->addAction(positionModeAct);
    m_menu_mode->addAction(jointModeAct);
    /**************************************************/

    /***************EDIT MENU*******************/
    QAction* undoAct = new QAction(tr("&Undo"), this);
    undoAct->setStatusTip(tr("Undo"));
    undoAct->setShortcut(QKeySequence(Qt::Key_U));
    connect(undoAct, SIGNAL(triggered()), this, SLOT(undo()));

    QAction* redoAct = new QAction(tr("&Redo"), this);
    redoAct->setStatusTip(tr("Redo"));
    redoAct->setShortcut(QKeySequence(Qt::Key_R));
    connect(redoAct, SIGNAL(triggered()), this, SLOT(redo()));

    m_menu_edit = menuBar()->addMenu(tr("&Edit"));

    m_menu_edit->addAction(undoAct);
    m_menu_edit->addAction(redoAct);
    /**************************************************/

    /***************OPTIONS MENU*******************/
    QAction* circleOptionAct = new QAction(tr("&Circle"), this);
    circleOptionAct->setStatusTip(tr("Circle"));
    circleOptionAct->setShortcut(QKeySequence(Qt::Key_C));
    connect(circleOptionAct, SIGNAL(triggered(bool)), this, SLOT(set_circle_option(bool)));

    QAction* zBufferAct = new QAction(tr("&Z-Buffer"), this);
    zBufferAct->setStatusTip(tr("Z-Buffer"));
    zBufferAct->setShortcut(QKeySequence(Qt::Key_Z));
    connect(zBufferAct, SIGNAL(triggered(bool)), this, SLOT(set_zbuffer_option(bool)));

    QAction* backFaceCullAct = new QAction(tr("&Backface Cull"), this);
    backFaceCullAct->setStatusTip(tr("Backface Cull"));
    backFaceCullAct->setShortcut(QKeySequence(Qt::Key_B));
    connect(backFaceCullAct, SIGNAL(triggered(bool)), this, SLOT(set_backfacecull_option(bool)));

    QAction* frontFaceCullAct = new QAction(tr("&Frontface Cull"), this);
    frontFaceCullAct->setStatusTip(tr("Frontface Cull"));
    frontFaceCullAct->setShortcut(QKeySequence(Qt::Key_F));
    connect(frontFaceCullAct, SIGNAL(triggered(bool)), this, SLOT(set_frontfacecull_option(bool)));

    QAction* coolAct = new QAction(tr("cool"), this);
    coolAct->setStatusTip(tr("cool"));
    connect(coolAct, SIGNAL(triggered(bool)), this, SLOT(set_cool_option(bool)));

    QActionGroup* optionGroup = new QActionGroup(this);
    circleOptionAct->setActionGroup(optionGroup);
    zBufferAct->setActionGroup(optionGroup);
    backFaceCullAct->setActionGroup(optionGroup);
    frontFaceCullAct->setActionGroup(optionGroup);
    coolAct->setActionGroup(optionGroup);

    optionGroup->setExclusive(false);

    circleOptionAct->setCheckable(true);
    zBufferAct->setCheckable(true);
    backFaceCullAct->setCheckable(true);
    frontFaceCullAct->setCheckable(true);
    coolAct->setCheckable(true);

    m_menu_options = menuBar()->addMenu(tr("&Options"));
    m_menu_options->addAction(circleOptionAct);
    m_menu_options->addAction(zBufferAct);
    m_menu_options->addAction(backFaceCullAct);
    m_menu_options->addAction(frontFaceCullAct);
    m_menu_options->addAction(coolAct);

    /**************************************************/
}

void AppWindow::reset_position(){
    m_viewer->resetPosition();
}

void AppWindow::reset_orientation(){
    m_viewer->resetOrientation();
}

void AppWindow::reset_joints(){
    m_viewer->resetJoints();
}

void AppWindow::reset_all(){
    m_viewer->resetAll();
}

void AppWindow::set_mode_position(){
    m_viewer->setMode(Viewer::Mode::POSITION);
}

void AppWindow::set_mode_joint(){
    m_viewer->setMode(Viewer::Mode::JOINT);
}

void AppWindow::undo(){
    m_viewer->undo();
}

void AppWindow::redo(){
    m_viewer->redo();
}

void AppWindow::set_frontfacecull_option(bool checked){
    m_viewer->setFrontFaceCull(checked);
}

void AppWindow::set_backfacecull_option(bool checked){
    m_viewer->setBackFaceCull(checked);
}

void AppWindow::set_zbuffer_option(bool checked){
    m_viewer->setZbuffer(checked);
}

void AppWindow::set_circle_option(bool checked){
    m_viewer->setTrackball(checked);
}

void AppWindow::set_cool_option(bool checked){
    m_viewer->setCool(checked);
}