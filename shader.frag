#version 130

in vec4 ADSLightIntensity;

uniform vec4 frag_color;
uniform bool pickingMode;

out vec4 finalColor;

void main()
{
	if(!pickingMode) finalColor = ADSLightIntensity;
	else finalColor = frag_color;
}
