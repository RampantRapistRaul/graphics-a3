#include "scene.hpp"
#include <iostream>
#include "Viewer.hpp"

Viewer* SceneNode::m_viewer = NULL;
float SceneNode::pickingColor = 1;

SceneNode::SceneNode(const std::string& name)
  : m_name(name)
{
  m_trans.setToIdentity();
  m_invtrans.setToIdentity();
}

SceneNode::~SceneNode()
{
}

void SceneNode::set_viewer(Viewer *viewer)
{
    SceneNode::m_viewer = viewer;
}

void SceneNode::walk_gl(bool picking) const
{
  ChildList::const_iterator iterator;
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
      SceneNode *child = *iterator;
      QMatrix4x4 temp_mtrans= child->get_transform();
      QMatrix4x4 temp_mtransform= child->get_transform_t();

      child->set_transform_t(m_trans, m_transformation * child->get_transform() * child->get_transform_t());
      child->walk_gl(picking);
      child->set_transform_t(temp_mtrans,temp_mtransform);
  }
}

void SceneNode::pickNode(QColor color){
  ChildList::const_iterator iterator;
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
      SceneNode *child = *iterator;
      child->pickNode(color);
  } 
}

void SceneNode::rotateAllJoints(double theta, char axis){
  ChildList::const_iterator iterator;
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
      SceneNode *child = *iterator;
      child->rotateAllJoints(theta,axis);
  } 
}

void SceneNode::resetAllJoints(){
  ChildList::const_iterator iterator;
  m_transformation.setToIdentity();
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
      SceneNode *child = *iterator;
      child->resetAllJoints();
  } 
}


void SceneNode::rotate(char axis, double angle)
{
  if(axis == 'x') m_trans.rotate(angle, 1, 0 ,0);
  else if(axis == 'y') m_trans.rotate(angle, 0, 1, 0);
  else if(axis == 'z') m_trans.rotate(angle, 0, 0, 1);
  else std::cerr << "Invalid use of SceneNode::rotate" << std::endl;
  m_invtrans = m_trans.inverted();
}

void SceneNode::scale(const Vector3D& amount)
{
  m_trans.scale(amount[0],amount[1],amount[2]);
  m_invtrans = m_trans.inverted();
}

void SceneNode::translate(const Vector3D& amount)
{
  m_trans.translate(amount[0],amount[1],amount[2]);
  m_invtrans = m_trans.inverted();
}

void SceneNode::qrotate(char axis, double angle)
{
  if(axis == 'x') m_transformation.rotate(angle, 1, 0 ,0);
  else if(axis == 'y') m_transformation.rotate(angle, 0, 1, 0);
  else if(axis == 'z') m_transformation.rotate(angle, 0, 0, 1);
  else std::cerr << "Invalid use of SceneNode::rotate" << std::endl;
  m_invtransformation = m_transformation.inverted();
}

void SceneNode::qscale(const Vector3D& amount)
{
  m_transformation.scale(amount[0],amount[1],amount[2]);
  m_invtransformation = m_transformation.inverted();
}

void SceneNode::qtranslate(const Vector3D& amount)
{
  m_transformation.translate(amount[0],amount[1],amount[2]);
  m_invtransformation = m_transformation.inverted();
}

bool SceneNode::is_joint() const
{
  return false;
}

JointNode::JointNode(const std::string& name)
  : SceneNode(name)
{
}

JointNode::~JointNode()
{
}

void JointNode::walk_gl(bool picking) const
{
  ChildList::const_iterator iterator;
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
    SceneNode *child = *iterator;
    QMatrix4x4 temp_mtrans= child->get_transform();
    QMatrix4x4 temp_mtransform= child->get_transform_t();

    child->set_transform_t(m_trans, m_transformation * child->get_transform() * child->get_transform_t());
    child->walk_gl(picking);

    child->set_transform_t(temp_mtrans, temp_mtransform);
  }
}

void JointNode::pickNode(QColor color){
  ChildList::const_iterator iterator;
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
      SceneNode *child = *iterator;
      child->pickNode(color);
  } 
}

void JointNode::rotateAllJoints(double theta, char axis){
  bool isJointRotated = false;
  ChildList::const_iterator iterator;
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {

      SceneNode *child = *iterator;
      GeometryNode* node = dynamic_cast<GeometryNode*>(child);
      if(node != 0){
        if(node->PICKED){
          isJointRotated = true;
        }
      }
      child->rotateAllJoints(theta,axis);

  } 
  if(isJointRotated){
    qrotate(axis,theta);
  }
}

void JointNode::resetAllJoints(){
  ChildList::const_iterator iterator;
  m_transformation.setToIdentity();
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
      SceneNode *child = *iterator;
      child->resetAllJoints();
  } 
}

bool JointNode::is_joint() const
{
  return true;
}

void JointNode::set_joint_x(double min, double init, double max){
  m_joint_x.min = min;
  m_joint_x.init = init;
  m_joint_x.max = max;
}

void JointNode::set_joint_y(double min, double init, double max)
{
  m_joint_y.min = min;
  m_joint_y.init = init;
  m_joint_y.max = max;
}

GeometryNode::GeometryNode(const std::string& name, Primitive* primitive)
  : SceneNode(name),
    m_primitive(primitive)
{
  uniquePickingColor = QColor(SceneNode::pickingColor * 5,SceneNode::pickingColor * 5,SceneNode::pickingColor * 5);
  SceneNode::pickingColor = SceneNode::pickingColor + 1;
  
  PICKED = false;
}

GeometryNode::~GeometryNode()
{
}

void GeometryNode::walk_gl(bool picking) const
{
  m_primitive->set_transform(m_trans * m_transformation);
  m_primitive->set_material(m_material);
  m_primitive->set_pick_color(uniquePickingColor);
  m_primitive->set_viewer(SceneNode::m_viewer);
  
  m_material->set_viewer(SceneNode::m_viewer);
  m_material->apply_gl();

  m_primitive->setPicked(PICKED);
  m_primitive->walk_gl(picking);

  ChildList::const_iterator iterator;
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
    SceneNode *child = *iterator;
    child->walk_gl(picking);
  }
}
 
void GeometryNode::pickNode(QColor color){
  ChildList::const_iterator iterator;
  if(color == uniquePickingColor){
    PICKED = !PICKED;
    return;
  }
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
      SceneNode *child = *iterator;
      child->pickNode(color);
  } 
}

void GeometryNode::rotateAllJoints(double theta, char axis){
  ChildList::const_iterator iterator;
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
      SceneNode *child = *iterator;
      child->rotateAllJoints(theta,axis);
  } 
}

void GeometryNode::resetAllJoints(){
  ChildList::const_iterator iterator;
  m_transformation.setToIdentity();
  for (iterator = m_children.begin(); iterator != m_children.end(); ++iterator) {
      SceneNode *child = *iterator;
      child->resetAllJoints();
  } 
}