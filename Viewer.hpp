#ifndef CS488_VIEWER_HPP
#define CS488_VIEWER_HPP

#include <QGLWidget>
#include <QGLShaderProgram>
#include <QMatrix4x4>
#include <QtGlobal>
#include "material.hpp"
#include "scene.hpp"

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#else 
#include <QGLBuffer>
#endif

class Viewer : public QGLWidget {
    
    Q_OBJECT
    friend class AppWindow;

public:
    Viewer(const QGLFormat& format, SceneNode *sceneRoot, QWidget *parent = 0);
    virtual ~Viewer();
    
    typedef enum {POSITION, JOINT} Mode;

    int x;
    QSize minimumSizeHint() const;
    QSize sizeHint() const;
    void setup_sphere(QColor q_material_diffuse, QColor q_material_specular, float shininess);
    void draw_sphere(QMatrix4x4 transformMatrix, QColor pickingColor, bool picking, bool PICKED);

    // If you want to render a new frame, call do not call paintGL(),
    // instead, call update() to ensure that the view gets a paint 
    // event.
  
protected:

    // Events we implement

    // Called when GL is first initialized
    virtual void initializeGL();
    // Called when our window needs to be redrawn
    virtual void paintGL();
    // Called when the window is resized (formerly on_configure_event)
    virtual void resizeGL(int width, int height);
    // Called when a mouse button is pressed
    virtual void mousePressEvent ( QMouseEvent * event );
    // Called when a mouse button is released
    virtual void mouseReleaseEvent ( QMouseEvent * event );
    // Called when the mouse moves
    virtual void mouseMoveEvent ( QMouseEvent * event );
    
    // Draw a circle for the trackball, with OpenGL commands.
    // Assumes the context for the viewer is active.
    void draw_trackball_circle();
    void vCalcRotVec(float fNewX, float fNewY,
                 float fOldX, float fOldY,
                 float fDiameter,
                 float &fVecX, float &fVecY, float &fVecZ);
    void setMode(Mode mode){m_mode = mode;}
    
    void undo();
    void redo();

    void resetPosition();
    void resetOrientation();
    void resetJoints();
    void resetAll();
    void setTrackball(bool checked);
    void setZbuffer(bool checked);
    void setFrontFaceCull(bool checked);
    void setBackFaceCull(bool checked);
    void setCool(bool checked);

private slots:
    void myUpdate();
    
private:

    QMatrix4x4 getCameraMatrix();
    void createSphereVertices();
    void translateWorld(float x, float y, float z);
    void rotateWorld(double theta, float x, float y, float z);
    void CULLED();
    void doUpdate();

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    QOpenGLBuffer mCircleBufferObject;
    QOpenGLVertexArrayObject mVertexArrayObject;
#else 
    QGLBuffer mCircleBufferObject;
#endif
    
    int mMvpMatrixLocation;
    int mNormalMatrixLocation;
    int mLightPositionLocation; 
    int mLightAmbientLocation; 
    int mLightDiffuseLocation;
    int mLightSpecularLocation; 
    int mMaterialAmbientLocation; 
    int mMaterialDiffuseLocation;
    int mMaterialSpecularLocation;
    int mMaterialShininessLocation; 
    int mColorLocation;
    int mPickingModeLocation;


    QMatrix4x4 mPerspMatrix;
    QMatrix4x4 mTranslateMatrix;
    QMatrix4x4 mRotateMatrix;
    QGLShaderProgram mProgram;
    std::vector<float> mSphereVerts;
    SceneNode *m_sceneRoot;
    QTimer* mTimer;
    Mode m_mode;
    bool m_trackBall;
    bool m_frontCulling;
    bool m_backCulling;
    bool m_cool;
    bool m_picking;
    bool m_zbuffer;


    //Mouse Events
    Qt::MouseButtons mouseButton;
    float mouseX;
    float mouseY;
    float originMouseX;
    float originMouseY;
    bool mousePressed;

};

#endif