#include <QtWidgets>
#include <QtOpenGL>
#include "Viewer.hpp"
#include <iostream>
#include <math.h>
#include <GL/glu.h>
#include "algebra.hpp"

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE 0x809D
#endif

Viewer::Viewer(const QGLFormat& format, SceneNode *sceneRoot, QWidget *parent) 
    : QGLWidget(format, parent) 
#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    , mCircleBufferObject(QOpenGLBuffer::VertexBuffer)
    , mVertexArrayObject(this)
#else 
    , mCircleBufferObject(QGLBuffer::VertexBuffer)
#endif
{
    x = 15;
    createSphereVertices();
    m_sceneRoot = sceneRoot;
    m_sceneRoot->set_viewer(this);

    mTimer = new QTimer(this);
    connect(mTimer, SIGNAL(timeout()), this, SLOT(myUpdate()));
    mTimer->start(1000/30);
    m_mode = POSITION;
    m_picking = false;
    m_cool = false;
}

Viewer::~Viewer() {
    // Nothing to do here right now.
}

QSize Viewer::minimumSizeHint() const {
    return QSize(50, 50);
}

QSize Viewer::sizeHint() const {
    return QSize(700, 700);
}

void Viewer::createSphereVertices(){
    for(int i = 0; i <= 60; i++)
    {
        double fillFactor = 2;
        double lat0 = fillFactor * M_PI * (-0.5 + (double) (i - 1) / 60);
        double height_z0  = sin(lat0);
        double width_z0 =  cos(lat0);

        double lat1 = fillFactor * M_PI * (-0.5 + (double) i / 60);
        double height_z1 = sin(lat1);
        double width_z1 = cos(lat1);

        for(int j = 0; j <= 60; j++)
        {
            double lng = 2 * M_PI * (double) (j - 1) / 60;
            double x = cos(lng);
            double y = sin(lng);

            mSphereVerts.push_back(x * width_z0); //X
            mSphereVerts.push_back(y * width_z0); //Y
            mSphereVerts.push_back(height_z0);    //Z

            mSphereVerts.push_back(x * width_z1); //X
            mSphereVerts.push_back(y * width_z1); //Y
            mSphereVerts.push_back(height_z1);    //Z

        }
    }
}

void Viewer::initializeGL() {
    QGLFormat glFormat = QGLWidget::format();
    if (!glFormat.sampleBuffers()) {
        std::cerr << "Could not enable sample buffers." << std::endl;
        return;
    }

    glShadeModel(GL_SMOOTH);
    glClearColor( 0.4, 0.4, 0.4, 0.0 );
    glEnable(GL_DEPTH_TEST);
    
    if (!mProgram.addShaderFromSourceFile(QGLShader::Vertex, "shader.vert")) {
        std::cerr << "Cannot load vertex shader." << std::endl;
        return;
    }

    if (!mProgram.addShaderFromSourceFile(QGLShader::Fragment, "shader.frag")) {
        std::cerr << "Cannot load fragment shader." << std::endl;
        return;
    }

    if ( !mProgram.link() ) {
        std::cerr << "Cannot link shaders." << std::endl;
        return;
    }

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    mVertexArrayObject.create();
    mVertexArrayObject.bind();

    mCircleBufferObject.create();
    mCircleBufferObject.setUsagePattern(QOpenGLBuffer::StaticDraw);
#else 
    /*
     * if qt version is less than 5.1, use the following commented code
     * instead of QOpenGLVertexVufferObject. Also use QGLBuffer instead of 
     * QOpenGLBuffer.
     */
    uint vao;
     
    typedef void (APIENTRY *_glGenVertexArrays) (GLsizei, GLuint*);
    typedef void (APIENTRY *_glBindVertexArray) (GLuint);
     
    _glGenVertexArrays glGenVertexArrays;
    _glBindVertexArray glBindVertexArray;
     
    glGenVertexArrays = (_glGenVertexArrays) QGLWidget::context()->getProcAddress("glGenVertexArrays");
    glBindVertexArray = (_glBindVertexArray) QGLWidget::context()->getProcAddress("glBindVertexArray");
     
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);    

    mCircleBufferObject.create();
    mCircleBufferObject.setUsagePattern(QGLBuffer::StaticDraw);

#endif

    if (!mCircleBufferObject.bind()) {
        std::cerr << "could not bind vertex buffer to the context." << std::endl;
        return;
    }

    mProgram.bind();

    mProgram.enableAttributeArray("vert");
    mProgram.setAttributeBuffer("vert", GL_FLOAT, 0, 3);

    mMvpMatrixLocation = mProgram.uniformLocation("mvpMatrix");
    mNormalMatrixLocation = mProgram.uniformLocation("normalMatrix");

    mLightPositionLocation = mProgram.uniformLocation("light_Position"); 
    mLightDiffuseLocation = mProgram.uniformLocation("light_Diffuse");
    mLightSpecularLocation = mProgram.uniformLocation("light_Specular"); 
    mMaterialAmbientLocation = mProgram.uniformLocation("material_Ambient"); 
    mMaterialDiffuseLocation = mProgram.uniformLocation("material_Diffuse");
    mMaterialSpecularLocation = mProgram.uniformLocation("material_Specular");
    mMaterialShininessLocation = mProgram.uniformLocation("material_Shininess"); 

    mPickingModeLocation = mProgram.uniformLocation("pickingMode");
    mColorLocation = mProgram.uniformLocation("frag_color");

    mProgram.setUniformValue(mPickingModeLocation, false);
    mProgram.setUniformValue(mLightPositionLocation, QVector4D(0,200,-155,0));
    mProgram.setUniformValue(mLightAmbientLocation, QColor(25,25,25));
    mProgram.setUniformValue(mLightDiffuseLocation, QColor(210,210,210));
    mProgram.setUniformValue(mLightSpecularLocation, QColor(225,225,225));

    m_zbuffer = false;
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    mTranslateMatrix.translate(0,0,-4);
}

void Viewer::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    
    if(m_trackBall) draw_trackball_circle();
    m_sceneRoot->walk_gl(false);
}

void Viewer::resizeGL(int width, int height) {
    if (height == 0) {
        height = 1;
    }

    mPerspMatrix.setToIdentity();
    mPerspMatrix.perspective(60.0, (float) width / (float) height, 0.001, 1000);

    glViewport(0, 0, width, height);
}

void Viewer::mousePressEvent ( QMouseEvent * event ) {
    originMouseX = event->x();
    originMouseY = event->y();
    mouseX = event->x();
    mouseY = event->y();
    mouseButton = event->buttons();
    mousePressed = true;
    if(mouseButton & Qt::LeftButton){
        if(m_mode == JOINT){
            bool tempTrack = m_trackBall;
            bool tempCool = m_cool;
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_CULL_FACE);
            m_picking = true;
            m_sceneRoot->walk_gl(true);
             
            // get color information from frame buffer
            unsigned char pixel[3];

            GLint viewport[4];
            glGetIntegerv(GL_VIEWPORT, viewport);

            glReadPixels(event->x(), viewport[3] - event->y(), 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);

            m_sceneRoot->pickNode(QColor(pixel[0],pixel[1],pixel[2]));

            // now our picked screen pixel color is stored in pixel[3]
            // so we search through our object list looking for the object that was selected
            
            m_sceneRoot->walk_gl(false);
            m_picking = false;
            CULLED();
            setZbuffer(m_zbuffer);
            m_cool = tempCool;
            m_trackBall = tempTrack;
        }
    }
}

void Viewer::mouseReleaseEvent ( QMouseEvent * event ) {
    mousePressed = false;
    mouseX = event->x();
}

void Viewer::mouseMoveEvent ( QMouseEvent * event ) {
    if(mousePressed){
        float deltaX = event->x() - mouseX;
        float deltaY = event->y() - mouseY;


        if(mouseButton & Qt::LeftButton){
            if(m_mode == POSITION){
                translateWorld(deltaX/30,-deltaY/30,0); 
            }
        }
        if(mouseButton & Qt::MidButton){
            if(m_mode == POSITION){
                float y = 1;
                if(deltaY < 0) y = -1;
                translateWorld(0,0,y/2);
            }
            else if (m_mode == JOINT && !m_picking){
                double theta = deltaY/2;
                QMatrix4x4 rotateMatrix;
                rotateMatrix.setToIdentity();
                rotateMatrix.rotate(theta,1,0,0);
                m_sceneRoot->rotateAllJoints(theta,'x');
                
            }
        }

        if(mouseButton & Qt::RightButton){
            if(m_mode == POSITION){
                float radius = width() < height() ? (float)width() * 0.25 : (float)height() * 0.25;
                float x;
                float y;
                float z;
                vCalcRotVec(event->x() - width()/2, -(event->y() - height()/2), mouseX - width()/2, -(mouseY - height()/2), radius, x, y, z);
                QVector3D rotateVector = QVector3D(x,y,z);
                double length = rotateVector.length();
                double theta = length * 180/M_PI;
                rotateWorld(theta,x,y,z);
            }
            else if (m_mode == JOINT && !m_picking ){
                double theta = deltaX/2;
                QMatrix4x4 rotateMatrix;
                rotateMatrix.setToIdentity();
                rotateMatrix.rotate(theta,0,1,0);
                m_sceneRoot->rotateAllJoints(theta,'y');
            }
        }

        mouseX = event->x();
        mouseY = event->y();
    }
}

QMatrix4x4 Viewer::getCameraMatrix() {
    // Todo: Ask if we want to keep this.
    QMatrix4x4 vMatrix;

    QMatrix4x4 cameraTransformation;
    QVector3D cameraPosition = cameraTransformation * QVector3D(0, 0, 20.0);
    QVector3D cameraUpDirection = cameraTransformation * QVector3D(0, 1, 0);

    vMatrix.lookAt(cameraPosition, QVector3D(0, 0, 0), cameraUpDirection);

    return mPerspMatrix * vMatrix * mTranslateMatrix * mRotateMatrix;
}

void Viewer::translateWorld(float x, float y, float z) {
    mTranslateMatrix.translate(x, y, z);
}

void Viewer::rotateWorld(double theta, float x, float y, float z) {
    mRotateMatrix.rotate(theta, x, y, z);
}

void Viewer::myUpdate(){
    doUpdate();
}

void Viewer::doUpdate(){
    if(!m_picking){
        update();
    }
}

void Viewer::draw_trackball_circle()
{
    float circleData[120];
    double radius = width() < height() ? (float)width() * 0.25 : (float)height() * 0.25;

    for(size_t i=0; i<40; ++i) {
        circleData[i*3] = radius * cos(i*2*M_PI/40);
        circleData[i*3 + 1] = radius * sin(i*2*M_PI/40);
        circleData[i*3 + 2] = 0.0;
    }
    mCircleBufferObject.allocate(circleData, 40 * 3 * sizeof(float));
    mCircleBufferObject.bind();

    int current_width = width();
    int current_height = height();

    // Set up for orthogonal drawing to draw a circle on screen.
    // You'll want to make the rest of the function conditional on
    // whether or not we want to draw the circle this time around.

    // Set orthographic Matrix
    QMatrix4x4 orthoMatrix;
    orthoMatrix.ortho(0.0, (float)current_width, 
           0.0, (float)current_height, -0.1, 0.1);

    // Translate the view to the middle
    QMatrix4x4 transformMatrix;
    transformMatrix.translate(width()/2.0, height()/2.0, 0.0);


    // Bind buffer object
    mCircleBufferObject.bind();
    mProgram.setUniformValue(mMvpMatrixLocation, orthoMatrix * transformMatrix);

    // Draw buffer
    glDrawArrays(GL_LINE_LOOP, 0, 40);

}

void Viewer::draw_sphere(QMatrix4x4 transformMatrix,QColor pickingColor, bool picking, bool PICKED)
{
    if(!picking){
        mProgram.setUniformValue(mPickingModeLocation, false);
    }
    else{
        mProgram.setUniformValue(mPickingModeLocation, true);
        mProgram.setUniformValue(mColorLocation, pickingColor);
    }

    if(PICKED){
        mProgram.setUniformValue(mMaterialDiffuseLocation, QColor(255,255,0));
    }

    mCircleBufferObject.allocate(&mSphereVerts[0], 60 * 60 * 2 * 3 * sizeof(float));
    mCircleBufferObject.bind();

    mProgram.setUniformValue(mMvpMatrixLocation, getCameraMatrix() * transformMatrix);
    mProgram.setUniformValue(mNormalMatrixLocation, (getCameraMatrix() * transformMatrix).normalMatrix());

    if(m_cool) glDrawArrays(GL_TRIANGLES, 0, 60*60*2);   
    else glDrawArrays(GL_TRIANGLE_STRIP, 0, 60*60*2);   

}

void Viewer::setup_sphere(QColor q_material_diffuse, QColor q_material_specular, float shininess){
    mProgram.setUniformValue(mMaterialAmbientLocation, QColor(15,15,15));
    mProgram.setUniformValue(mMaterialDiffuseLocation, q_material_diffuse);
    mProgram.setUniformValue(mMaterialSpecularLocation, q_material_specular);
    mProgram.setUniformValue(mMaterialShininessLocation, shininess);
}

void Viewer::undo(){

    doUpdate();
}

void Viewer::redo(){

    doUpdate();
}

void Viewer::resetPosition(){
    mTranslateMatrix.setToIdentity();
    mTranslateMatrix.translate(0,0,-4);
    doUpdate();
}

void Viewer::resetOrientation(){
    mRotateMatrix.setToIdentity();
    doUpdate();
}

void Viewer::resetJoints(){
    m_sceneRoot->resetAllJoints();
    doUpdate();
}

void Viewer::resetAll(){
    resetPosition();
    resetOrientation();
    resetJoints();
    doUpdate();
}

void Viewer::setTrackball(bool checked){
    m_trackBall = checked;
    doUpdate();
}

void Viewer::setZbuffer(bool checked){
    m_zbuffer = checked;
    if(checked){
        glEnable(GL_DEPTH_TEST);
    }
    else{
        glDisable(GL_DEPTH_TEST);
    }
    doUpdate();
}

void Viewer::CULLED(){
    if(m_frontCulling && m_backCulling){
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT_AND_BACK);
    }
    else if(m_frontCulling){
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
    }
    else if(m_backCulling){
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);   
    }
    else{
        glDisable(GL_CULL_FACE);
    }
    doUpdate();
}

void Viewer::setFrontFaceCull(bool checked){
    m_frontCulling = checked;
    CULLED();
}

void Viewer::setBackFaceCull(bool checked){
    m_backCulling = checked;
    CULLED();
}

void Viewer::setCool(bool checked){
    m_cool = checked;
    doUpdate();
}

/*******************************************************
 * 
 * void vCalcRotVec(float fNewX, float fNewY, 
 *                  float fOldX, float fOldY,
 *                  float fDiameter,
 *                  float *fVecX, float *fVecY, float *fVecZ);
 *
 *    Calculates a rotation vector based on mouse motion over
 *    a virtual trackball.
 *
 *    The fNew and fOld mouse positions
 *    should be in 'trackball' space. That is, they have been
 *    transformed into a coordinate system centered at the middle
 *    of the trackball. fNew, fOld, and fDiameter must all be specified
 *    in the same units (pixels for example).
 *
 * Parameters: fNewX, fNewY - New mouse position in trackball space.
 *                            This is the second point along direction
 *                            of rotation.
 *             fOldX, fOldY - Old mouse position in trackball space.
 *                            This is the first point along direction
 *                            of rotation.
 *             fDiameter - Diameter of the trackball. This should
 *                         be specified in the same units as fNew and fOld.
 *                         (ie, usually pixels if fNew and fOld are transformed
 *                         mouse positions)
 *             fVec - The output rotation vector. The length of the vector
 *                    is proportional to the angle of rotation.
 *
 *******************************************************/

void Viewer::vCalcRotVec(float fNewX, float fNewY,
                 float fOldX, float fOldY,
                 float fDiameter,
                 float &fVecX, float &fVecY, float &fVecZ) {
   float fNewVecX, fNewVecY, fNewVecZ,        /* Vector corresponding to new mouse location */
         fOldVecX, fOldVecY, fOldVecZ,        /* Vector corresponding to old mouse location */
         fLength;

   /* Vector pointing from center of virtual trackball to
    * new mouse position
    */
   fNewVecX    = fNewX * 2.0 / fDiameter;
   fNewVecY    = fNewY * 2.0 / fDiameter;
   fNewVecZ    = (1.0 - fNewVecX * fNewVecX - fNewVecY * fNewVecY);

   /* If the Z component is less than 0, the mouse point
    * falls outside of the trackball which is interpreted
    * as rotation about the Z axis.
    */
   if (fNewVecZ < 0.0) {
      fLength = sqrt(1.0 - fNewVecZ);
      fNewVecZ  = 0.0;
      fNewVecX /= fLength;
      fNewVecY /= fLength;
   } else {
      fNewVecZ = sqrt(fNewVecZ);
   }

   /* Vector pointing from center of virtual trackball to
    * old mouse position
    */
   fOldVecX    = fOldX * 2.0 / fDiameter;
   fOldVecY    = fOldY * 2.0 / fDiameter;
   fOldVecZ    = (1.0 - fOldVecX * fOldVecX - fOldVecY * fOldVecY);
 
   /* If the Z component is less than 0, the mouse point
    * falls outside of the trackball which is interpreted
    * as rotation about the Z axis.
    */
   if (fOldVecZ < 0.0) {
      fLength = sqrt(1.0 - fOldVecZ);
      fOldVecZ  = 0.0;
      fOldVecX /= fLength;
      fOldVecY /= fLength;
   } else {
      fOldVecZ = sqrt(fOldVecZ);
   }

   /* Generate rotation vector by calculating cross product:
    * 
    * fOldVec x fNewVec.
    * 
    * The rotation vector is the axis of rotation
    * and is non-unit length since the length of a crossproduct
    * is related to the angle between fOldVec and fNewVec which we need
    * in order to perform the rotation.
    */
   fVecX = fOldVecY * fNewVecZ - fNewVecY * fOldVecZ;
   fVecY = fOldVecZ * fNewVecX - fNewVecZ * fOldVecX;
   fVecZ = fOldVecX * fNewVecY - fNewVecX * fOldVecY;
}